// #region Attributes //

// Requires.
const autoprefixer    = require( "autoprefixer" );
const browserify      = require( "browserify" );
const buffer          = require( "vinyl-buffer" );
const csso            = require( "gulp-csso" );
const del             = require( "del" );
const gulp            = require( "gulp" );
const gulpif          = require( "gulp-if" );
const htmlmin         = require( "gulp-htmlmin" );
const postcss         = require( "gulp-postcss" );
const replace         = require( "gulp-replace" );
const runsequence     = require( "run-sequence" );
const sass            = require( "gulp-sass" );
const source          = require( "vinyl-source-stream" );
const tsify           = require( "tsify" );
const terser          = require( "gulp-terser" );
const watchify        = require( "watchify" );

var Readable = require( 'stream' ).Readable;

// Constants
// Paths.
const kTsSrcEntryPath = "src/Main.ts";
const kTsOutDevPath = "bin";
const kTsOutReleasePath = "release";
const kScssOutMainDevPath = "bin/css";

// Enums.
const publish_type = {
    RELEASE: 0,
    DEBUG: 1,
    LOCALHOST: 2 };

// #endregion //


// #region Methods //

// Development build.
function buildDebug()
{
    // It compiles .ts files into .js and copies the bundled file to the bin folder.
    return browserify( {
            basedir: ".",
            debug: true,
            entries: [ kTsSrcEntryPath ],
            cache: {},
            packageCache: {} } )
        .plugin( tsify )
        .bundle()
        .pipe( source( "game.js" ) )
        .pipe( buffer() )
        .pipe( gulp.dest( kTsOutDevPath ) );
}

function watchDebug()
{
    let b = browserify( {
        basedir: ".",
        debug: true,
        entries: [ kTsSrcEntryPath ],
        cache: {},
        packageCache: {} } );
    b.plugin( watchify );
    b.plugin( tsify );
    b.on( "update", bundle );
    b.on( "log", onLog );
    b.on( "error", console.error );
    bundle();

    console.log( "Completing starting bundle, please wait..." );

    function bundle() 
    {
        b.bundle()
            .pipe( source( "game.js" ) )
            .pipe( buffer() )
            .pipe( gulp.dest( kTsOutDevPath ) );
    };

    function onLog( message )
    {
        console.log( message + " - " + new Date().toLocaleString() );
    };
}

// Release build.
function cleanRelease()
{
    const arrPath = [ 
        kTsOutReleasePath + "/*.js",
        kTsOutReleasePath + "/*.html",
        kTsOutReleasePath + "/resources/**",
        kTsOutReleasePath + "/css/**" ];
    return del( arrPath, { force: true } );
}

function publish( publishType )
{
    // Copy html.
    gulp.src( [ kTsOutDevPath + "/index.html" ], { nocase: false } )
        .pipe( htmlmin( { collapseWhitespace: true, removeComments: true } ) )
        .pipe( gulp.dest( kTsOutReleasePath ) );

    // Copy css.
    gulp.src( [ kTsOutDevPath + "/css/**" ], { nocase: false } )
        .pipe( csso() )
        .pipe( gulp.dest( kTsOutReleasePath + "/css" ) );

    // Copy resources.
    gulp.src( [ kTsOutDevPath + "/resources/**" ], { nocase: false } )
        .pipe( gulp.dest( kTsOutReleasePath + "/resources" ) );

    // It compiles .ts files into .js, bundles everything together, minifies it and copies it to the release folder.
    return browserify( {
        basedir: ".",
        debug: false,
        entries: [ kTsSrcEntryPath ],
        cache: {},
        packageCache: {} } )
        .plugin( tsify )
        .bundle( function( err, src) {
            if ( err )
            {
                console.log( err );
            }
            else
            {
                let r = new Readable();
                r.push( src );    
                r.push( null ); // end of file.
                r.pipe( source( "game.js" ) )
                    .pipe( buffer() )
                    .pipe( gulpif( publishType == publish_type.RELEASE, terser() ) ) 
                    .pipe( gulp.dest( kTsOutReleasePath ) );
                }
        } );
}

// SASS build.
function buildSass() 
{
    return gulp.src( [ "scss/*" ], { nocase: false } )
        .pipe( sass() )
        .pipe( postcss( [ autoprefixer() ] ) )
        .pipe( gulp.dest( kScssOutMainDevPath ) );
}

// #endregion //


// #region Tasks //

gulp.task( "build_sass", buildSass );

// Debug.
// Game.
gulp.task( "build_debug", buildDebug );
gulp.task( "watch_debug", watchDebug );
// Release.
gulp.task( "publish_release", () => { runsequence( "clean_release", "build_release" ); } );
gulp.task( "publish_debug", () => { runsequence( "clean_release", "build_release_no_uglify" ); } );
gulp.task( "publish_localhost", () => { runsequence( "clean_release", "build_release_for_localhost" ); } );
gulp.task( "clean_release", cleanRelease );
gulp.task( "build_release", publish.bind( this, publish_type.RELEASE ) );
gulp.task( "build_release_no_uglify", publish.bind( this, publish_type.DEBUG ) );
gulp.task( "build_release_for_localhost", publish.bind( this, publish_type.LOCALHOST ) );

// #endregion //