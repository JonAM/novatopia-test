import * as PIXI from "pixi.js";

import Grid from "./Grid";


class Main
{
    // #region Attributes //

    private _app: PIXI.Application = null;
    private _grid: Grid = null;

    // #endregion //


    // #region Methods //

    public constructor()
    { 
        this._app = new PIXI.Application();
        let mainLayer: HTMLDivElement = document.getElementById( "mainLayer" ) as HTMLDivElement;
        mainLayer.append( this._app.view );

        this.onCreateGridBtn_Click();

        PIXI.Ticker.shared.add( this.onUpdate.bind( this ) );

        ( document.getElementById( "createGridBtn" ) as HTMLButtonElement ).addEventListener( "click", this.onCreateGridBtn_Click.bind( this ) );
    }

        private createGrid( width: number, height: number ): Grid
        {
            let result: Grid = new Grid();
            result.app = this._app;
            result.colCount = width;
            result.rowCount = height;
            result.init();

            return result;
        }

    // #endregion //


    // #region Callbacks //

    private onUpdate( dt: number ): void
    {
        if ( this._grid )
        {
            this._grid.update( dt );
        }
    }

    private onCreateGridBtn_Click(): void
    {
        const kGridWidth: number = Number.parseInt( ( document.getElementById( "gridWidth" ) as HTMLInputElement ).value );
        const kGridHeight: number = Number.parseInt( ( document.getElementById( "gridHeight" ) as HTMLInputElement ).value );
        if ( kGridWidth >= 1 && kGridHeight >= 1 )
        {
            if ( this._grid )
            {
                this._grid.end();
                this._grid.destroy( { children: true } );
            }

            this._grid = this.createGrid( kGridWidth, kGridHeight ); // set grid's width and height here!
            this._grid.x = this._grid.rowCount * this._grid.tileWidth * 0.5;
            this._app.stage.addChild( this._grid );

            this._app.renderer.resize( 
                ( this._grid.rowCount + this._grid.colCount ) * this._grid.tileWidth * 0.5, 
                this._grid.height );
        }
    }

    // #endregion //
}

// When the index page has finished loading, create our app.
window.onload = () =>
{
    var main: Main = new Main();
}