import * as PIXI from "pixi.js";


/**
 * The grid is a top-down representation of the isometric tile map.
 * The top-left corner is used as origin. Positive x values go right. Positive y values go down.
 */
export default class Grid extends PIXI.Container
{
    // #region Attributes //

    private _app: PIXI.Application = null;

    private _lastHoveredTile: PIXI.Point = null;
    private _lastSelectedTile: PIXI.Point = null;

    // Drawing layers.
    private _arrGraphics: Array<PIXI.Graphics> = null;

    private _rowCount: number = null;
    private _colCount: number = null;

    // Constants.
    private readonly _kTileWidth: number = 132;

    // #endregion //


    // #region Properties //

    public set app( value: PIXI.Application ) { this._app = value; }
    public set colCount( value: number ) { this._colCount = value; }
    public set rowCount( value: number ) { this._rowCount = value; }

    public get rowCount(): number { return this._rowCount; }
    public get colCount(): number { return this._colCount; }
    public get tileWidth(): number { return this._kTileWidth; }

    // #endregion //


    // #region Methods //

    public constructor() 
    {
        super();

        this._arrGraphics = [ 
            new PIXI.Graphics(), // MAIN.
            new PIXI.Graphics(), // SELECTION.
            new PIXI.Graphics() ]; // HOVER.
    }

    public init(): void
    {
        console.assert( this._app != null, "Grid.ts :: init() :: this._app cannot be null." );
        console.assert( this._rowCount > 0, "Grid.ts :: init() :: this._rowCount must be > 0." );
        console.assert( this._colCount > 0, "Grid.ts :: init() :: this._colCount must be > 0." );

        this._lastHoveredTile = new PIXI.Point( -1 );
        this._lastSelectedTile = new PIXI.Point( -1 );

        this._arrGraphics[ layer_id.MAIN ].interactive = true;
        this._arrGraphics[ layer_id.MAIN ].buttonMode = true;
        this.addChild( this._arrGraphics[ layer_id.MAIN ] );
        this.addChild( this._arrGraphics[ layer_id.SELECTION ] );
        this.addChild( this._arrGraphics[ layer_id.HOVER ] );

        this.drawMainLayer();

        // Listen to events.
        this._arrGraphics[ layer_id.MAIN ].on( "click", this.onClick, this)
    }

        private drawMainLayer(): void
        {
            let g: PIXI.Graphics = this._arrGraphics[ layer_id.MAIN ];
            g.clear();
            g.lineStyle( 0.5, 0xffd900, 1 );

            for ( let i: number = 0; i < this._rowCount; ++i )
            {
                for ( let j: number = 0; j < this._colCount; ++j )
                {
                    let tileIsoTopLeft: PIXI.Point = this.toIsoProj( j * this._kTileWidth, i * this._kTileWidth );
                    let tileIsoTopRight: PIXI.Point = this.toIsoProj( ( j + 1 ) * this._kTileWidth, i * this._kTileWidth );
                    let tileIsoBotRight: PIXI.Point = this.toIsoProj( ( j + 1 ) * this._kTileWidth, ( i + 1 ) * this._kTileWidth );
                    let tileIsoBotLeft: PIXI.Point = this.toIsoProj( j * this._kTileWidth, ( i + 1 ) * this._kTileWidth );

                    g.beginFill( i == 0 || i == this._rowCount - 1 || j == 0 || j == this._colCount - 1 ? 0x006400 : 0xc0c0c0 );
                    g.moveTo( tileIsoTopLeft.x, tileIsoTopLeft.y );
                    g.lineTo( tileIsoTopRight.x, tileIsoTopRight.y );
                    g.lineTo( tileIsoBotRight.x, tileIsoBotRight.y );
                    g.lineTo( tileIsoBotLeft.x, tileIsoBotLeft.y );
                    g.lineTo( tileIsoTopLeft.x, tileIsoTopLeft.y );
                    g.endFill();
                }
            }
        }

            private toIsoProj( orthoX: number, orthoY: number ): PIXI.Point
            {
                return new PIXI.Point( ( orthoX - orthoY ) * 0.5, ( orthoX + orthoY ) * 0.25 );
            }

    public end(): void
    {
        // Cleanup events.
        this._arrGraphics[ layer_id.MAIN ].off( "click", this.onClick, this );

        this._app = null;
        this._lastHoveredTile = null;
        this._lastSelectedTile = null;
        this._arrGraphics = null;
        this._colCount = null;
        this._rowCount = null;
    }

    public update( dt: number ): void
    {
        const kCursor: PIXI.Point = new PIXI.Point(
            this._app.renderer.plugins.interaction.mouse.global.x - this.x,
            this._app.renderer.plugins.interaction.mouse.global.y - this.y );
        const kOrthoPos: PIXI.Point = this.toOrthoProj( kCursor.x, kCursor.y );

        let hoveredTile: PIXI.Point = this.findTile( kOrthoPos.x, kOrthoPos.y );
        if ( hoveredTile )
        {
            if ( this._lastHoveredTile.x != hoveredTile.x || this._lastHoveredTile.y != hoveredTile.y )
            {
                this.hoverTile( hoveredTile, 0xffcc00, layer_id.HOVER );
                this._lastHoveredTile.copyTo( hoveredTile );
            }
        }
        else
        {
            this._lastHoveredTile.set( -1 );
            this._arrGraphics[ layer_id.HOVER ].clear();
        }
    }
    
        private toOrthoProj( isoX: number, isoY: number ): PIXI.Point
        {
            return new PIXI.Point( isoX + isoY * 2, isoY * 2 - isoX );
        }

        private findTile( orthoX: number, orthoY: number ): PIXI.Point
        {
            let result: PIXI.Point = null;

            if ( orthoX >= 0 
                && orthoY >= 0
                && orthoX < this._colCount * this._kTileWidth 
                && orthoY < this._rowCount * this._kTileWidth )
            {
                result = new PIXI.Point( 
                    Math.floor( orthoX / this._kTileWidth ),
                    Math.floor( orthoY / this._kTileWidth ) );
            }

            return result;
        }

        private hoverTile( tile: PIXI.Point, color: number, layerId: layer_id ): void
        {
            const kTileMargin: number = 5;
            let tileIsoTopLeft: PIXI.Point = this.toIsoProj( tile.x * this._kTileWidth + kTileMargin, tile.y * this._kTileWidth + kTileMargin );
            let tileIsoTopRight: PIXI.Point = this.toIsoProj( ( tile.x + 1 ) * this._kTileWidth - kTileMargin, tile.y * this._kTileWidth + kTileMargin );
            let tileIsoBotRight: PIXI.Point = this.toIsoProj( ( tile.x + 1 ) * this._kTileWidth - kTileMargin, ( tile.y + 1 ) * this._kTileWidth - kTileMargin );
            let tileIsoBotLeft: PIXI.Point = this.toIsoProj( tile.x * this._kTileWidth + kTileMargin, ( tile.y + 1 ) * this._kTileWidth - kTileMargin );

            let g: PIXI.Graphics = this._arrGraphics[ layerId ];
            g.clear();
            g.lineStyle( 2, color, 1 );
            g.moveTo( tileIsoTopLeft.x, tileIsoTopLeft.y );
            g.lineTo( tileIsoTopRight.x, tileIsoTopRight.y );
            g.lineTo( tileIsoBotRight.x, tileIsoBotRight.y );
            g.lineTo( tileIsoBotLeft.x, tileIsoBotLeft.y );
            g.lineTo( tileIsoTopLeft.x, tileIsoTopLeft.y );
        }

    // #endregion //


    // #region Callbacks //

    private onClick(): void
    {
        const kCursor: PIXI.Point = new PIXI.Point(
            this._app.renderer.plugins.interaction.mouse.global.x - this.x,
            this._app.renderer.plugins.interaction.mouse.global.y - this.y );
        const kOrthoPos: PIXI.Point = this.toOrthoProj( kCursor.x, kCursor.y );

        let selectedTile: PIXI.Point = this.findTile( kOrthoPos.x, kOrthoPos.y );
        if ( selectedTile 
            && ( this._lastSelectedTile.x != selectedTile.x 
                || this._lastSelectedTile.y != selectedTile.y ) )
        {
            this.selectTile( selectedTile, 0xff0000, layer_id.SELECTION );
            this._lastSelectedTile.copyFrom( selectedTile );
        }
    }

        private selectTile( tile: PIXI.Point, color: number, layerId: layer_id ): void
        {
            let tileIsoTopLeft: PIXI.Point = this.toIsoProj( tile.x * this._kTileWidth, tile.y * this._kTileWidth );
            let tileIsoTopRight: PIXI.Point = this.toIsoProj( ( tile.x + 1 ) * this._kTileWidth, tile.y * this._kTileWidth );
            let tileIsoBotRight: PIXI.Point = this.toIsoProj( ( tile.x + 1 ) * this._kTileWidth, ( tile.y + 1 ) * this._kTileWidth );
            let tileIsoBotLeft: PIXI.Point = this.toIsoProj( tile.x * this._kTileWidth, ( tile.y + 1 ) * this._kTileWidth );

            let g: PIXI.Graphics = this._arrGraphics[ layerId ];
            g.clear();
            g.lineStyle( 1, color );
            g.beginFill( color );
            g.moveTo( tileIsoTopLeft.x, tileIsoTopLeft.y );
            g.lineTo( tileIsoTopRight.x, tileIsoTopRight.y );
            g.lineTo( tileIsoBotRight.x, tileIsoBotRight.y );
            g.lineTo( tileIsoBotLeft.x, tileIsoBotLeft.y );
            g.lineTo( tileIsoTopLeft.x, tileIsoTopLeft.y );
            g.endFill();
        }

    // #endregion //
}

export const enum layer_id
{
    MAIN = 0,
    SELECTION,
    HOVER
}